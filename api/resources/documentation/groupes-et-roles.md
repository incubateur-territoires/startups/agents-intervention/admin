# Groupes et rôles

##  Les groupes

### Agent
Un agent du service technique peut :
- voir la liste des interventions qui lui sont affectées;
- voir le détail d'une intervention qui lui est affectée;
- créer une intervention sans l'affecter;
- ajouter une photo à une intervention;
- ajouter un commentaire à une intervention;
- changer le statut d'une intervention.

### Direction
Un directeur de service technique peut :
- voir toutes les interventions;
- ajouter une intervention;
- modifier une intervention;
- assigner une intervention à un ou plusieurs agents;
- supprimer une intervention.

### Élu
Un élu peut :
- tout voir (il n'a qu'un rôle de mesure de l'action).
